"""Implements text processing pipeline."""

import argparse
from pathlib import Path

from box import ConfigBox

from hat_client.text.process import process_nltk_text
from hat_client.utils.config import load_config
from hat_client.utils.logger import get_logger


def text_processing(config: ConfigBox) -> None:
    """Text processing pipeline.

    Args:
        config (ConfigBox): Configuration settings.
    """
    logger = get_logger('TEXT_PROCESSING', config.base.log_level)

    logger.info('Process text')
    file_path = Path(config.data_preprocessing.output_file)
    output_file_path = Path(config.text_processing.output_file)
    process_nltk_text(file_path, output_file_path)
    logger.info(f'Processed text is saved to {output_file_path}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    config_path = Path(args.config)
    config = load_config(config_path)

    text_processing(config)
