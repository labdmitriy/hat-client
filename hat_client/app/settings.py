"""Application settings."""

import os
from pathlib import Path
from typing import Any, Dict

import nltk
from nltk.stem import WordNetLemmatizer

from hat_client.models.io import load_model, load_vocab
from hat_client.utils.config import load_config
from hat_client.utils.file import get_project_root


def setup_nltk() -> None:
    """Configure nltk package."""
    nltk.download('wordnet')
    nltk.download('stopwords')
    nltk.download('punkt')

    # Warmup wordnet to avoid response delays in the game
    lemmatizer = WordNetLemmatizer()
    lemmatizer.lemmatize('warmup')


def setup_app() -> Dict[str, Any]:
    """Set up application.

    Returns:
        Dict[str, Any]: Objects for application running.
    """
    setup_nltk()

    project_root = get_project_root()

    config_env_var = os.environ.get('CONFIG_PATH')
    if config_env_var:
        config_path = Path(str(config_env_var))
    else:
        config_path = project_root / Path('params.yaml')
    config = load_config(config_path)

    model_path = project_root / Path(config.model_training.output_file)
    package_name = config.model_training.package_name
    model = load_model(package_name, model_path)

    hat_vocab_path = project_root / Path(config.data_preprocessing.hat_vocab_file)
    hat_vocab = load_vocab(hat_vocab_path)

    settings = {'model': model, 'hat_vocab': hat_vocab}

    return settings


app_config = setup_app()
