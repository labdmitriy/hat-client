"""Configuration operations."""

from pathlib import Path

import box
import yaml


def load_config(config_path: Path) -> box.ConfigBox:
    """Loads yaml config in instance of box.ConfigBox.

    Args:
        config_path (Path): Path to config.

    Returns:
        box.ConfigBox
    """
    with open(config_path) as f:
        config = yaml.safe_load(f)
        config = box.ConfigBox(config)

    return config
