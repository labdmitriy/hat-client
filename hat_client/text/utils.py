"""Helper methods for text processing."""

from collections import Counter
from pathlib import Path
import pickle
import re
from typing import Dict, List, Union

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize, word_tokenize
import smart_open

LEMMATIZER = WordNetLemmatizer()
STOP_WORDS = set(stopwords.words('english'))


def convert_document_to_sentences(doc: str) -> List[str]:
    """Tokenize document to sentences.

    Args:
        doc (str): Document to convert.

    Returns:
        List[str]: Tokenized sentences.
    """
    return sent_tokenize(doc)


def convert_document_to_words(doc: str,
                              lemmatize: bool = False,
                              min_word_length: int = 1) -> List[str]:
    """Tokenize document to words.

    Args:
        doc (str): Document to convert.
        lemmatize (bool): Whether to lemmatize the word after tokenization. Defaults to False.
        min_word_length (int): Minimum length of the word to include for processing. Defaults to 1.

    Returns:
        List[str]: Tokenized words.
    """
    pattern = re.compile(r'[^a-z]+')
    doc = re.sub(pattern, ' ', doc.lower())

    # Lemmatization is applied as used in the game, but for correct lemmatization
    # POS tags are required

    if lemmatize:
        words = [
            LEMMATIZER.lemmatize(word)
            for word in word_tokenize(doc, preserve_line=True)
            if ((word not in STOP_WORDS) and len(word.strip()) >= min_word_length)
        ]
    else:
        words = [
            word for word in word_tokenize(doc, preserve_line=True)
            if ((word not in STOP_WORDS) and len(word.strip()) >= min_word_length)
        ]

    return words


def prepare_hat_vocab(url: str, vocab_path: Path) -> Dict[str, Union[int, float]]:
    """Prepare possible words for the hat.

    Args:
        url (str): URL of the corpus to process.
        vocab_path (Path): Path to save vocabulary for the hat.

    Returns:
        Dict[str, Union[int, float]]: Hat vocabulary statistics.
    """
    word_counter: Counter = Counter()
    with smart_open.open(url, 'r', encoding='utf-8') as f:
        for doc in f:
            words = convert_document_to_words(doc, min_word_length=4)
            word_counter.update(words)

    max_count = max(count for word, count in word_counter.items()) / 10
    min_count = max([10, max_count / 100])

    hat_words = set(
        [word for word, count in word_counter.items() if (min_count < count <= max_count)])

    with open(vocab_path, 'wb') as f:
        pickle.dump(hat_words, f)

    hat_vocab_stats = {
        'vocab_size': len(hat_words),
        'min_count': min_count,
        'max_count': max_count
    }

    return hat_vocab_stats


def remove_repeated_words(words: List[str]) -> List[str]:
    """Remove repeated words with preserving of the order.

    Args:
        words (List[str]): Words for processing.

    Returns:
        List[str]: Unique words.
    """
    # unique_words: List[str] = []

    # for word in words:
    #     if word not in set(unique_words):
    #         unique_words.append(word)

    # return unique_words

    return list(dict.fromkeys(words).keys())
