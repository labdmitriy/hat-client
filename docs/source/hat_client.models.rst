hat\_client.models package
==========================

.. automodule:: hat_client.models
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.models.io module
----------------------------

.. automodule:: hat_client.models.io
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.models.train module
-------------------------------

.. automodule:: hat_client.models.train
   :members:
   :undoc-members:
   :show-inheritance:
