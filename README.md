## The Hat Game Client

### Description
Client for the [Hat game](https://gitlab.com/production-ml/the-hat-game), created for [Production ML course](https://github.com/data-mining-in-action/DMIA_ProductionML_2021_Spring).  
Presentation: https://hackmd.io/@labdmitriy/the-hat-game-solution  
Documentation: https://hat-client.readthedocs.io/  
    
### How to use
- Install Python 3.8.5 and poetry
- Create directories for data, models and logs: `make dirs`
- Install required packages: `make requirements`
- Run pipeline: `dvc repro --no-commit`
- Upload model and hat vocabulary to the file transfer service (e.g., transfer.sh or oshi.at) and get the file links
- Update links in Dockerfile.app for the model and hat vocabulary
- Commit and push updated git repository to remote git host: `git push origin master`
- Configure heroku for deployment using Docker and remote for heroku (`<heroku_remote>`)
- Push updated repository to heroku: `push origin <heroku_remote> master`
- Check heroku logs after deployment: `heroku logs -t`
- Open heroku application in browser: `heroku open`




