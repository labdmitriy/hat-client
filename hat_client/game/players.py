"""Implements game players and its interface."""

from abc import ABC, abstractmethod
from collections import namedtuple
from copy import deepcopy
from operator import itemgetter
import re
from typing import Any, List, Set

from gensim.models.keyedvectors import Word2VecKeyedVectors
from Levenshtein import distance
import requests

from hat_client.text.utils import convert_document_to_words

PlayerDefinition = namedtuple('PlayerDefinition', ['name', 'api'])


class AbstractPlayer(ABC):
    """Abstract class for creating players."""

    @abstractmethod
    def explain(self, word: str, n_words: int) -> List[str]:
        """Explain given word using specific number of words.

        Args:
            word (str): Word to explain.
            n_words (int): Number of words required in explanation.

        Returns:
            List[str]: Words to explain given word.
        """
        pass

    @abstractmethod
    def guess(self, words: List[str], n_words: int) -> List[str]:
        """Guess the word using provided list of explaining words.

        Args:
            words (List[str]): Explaining words.
            n_words (int): Required number of guesses.

        Returns:
            List[str]: Possible words for guessing.
        """
        pass


class RemotePlayer(AbstractPlayer):
    """Remote player implementation."""

    def __init__(self, url: str, timeout: int = 1) -> None:
        """Initialize remote player.

        Args:
            url (str): URL for remote player access
            timeout (int, optional): Timeout to wait for explain/guess response. Defaults to 1.
        """
        self.url = url
        self.timeout = timeout
        self.ping()

    def ping(self) -> None:
        """Ping remote player for warmup."""
        try:
            response = requests.get(self.url)
            assert response.status_code == 200
        except Exception:
            pass

    def explain(self, word: str, n_words: int) -> List[str]:
        """Explain given word using specific number of words.

        Args:
            word (str): Word to explain.
            n_words (int): Number of words required in explanation.

        Returns:
            List[str]: Words to explain given word.
        """
        try:
            response = requests.get(
                self.url + '/explain',
                {
                    'word': word,
                    'n_words': str(n_words)
                },
                timeout=self.timeout,
            )
            word_list = response.json()
        except Exception:
            word_list = []
        return word_list

    def guess(self, words: List[str], n_words: int) -> List[str]:
        """Guess the word using provided list of explaining words.

        Args:
            words (List[str]): Explaining words.
            n_words (int): Required number of guesses.

        Returns:
            List[str]: Possible words for guessing.
        """
        try:
            response = requests.get(
                self.url + '/guess',
                {
                    'words': words,
                    'n_words': str(n_words)
                },
                timeout=self.timeout,
            )
            word_list = response.json()
        except Exception:
            word_list = []
        return word_list


class LocalFasttextPlayer(AbstractPlayer):
    """Local player based on original fasttext model."""

    def __init__(self, model: Any) -> None:
        """Initialize player with fasttext model."""
        self.model = model

    def _find_words_for_sentence(self, sentence: str, n_closest: int) -> List[str]:
        """Find top closest words for sentence.

        Args:
            sentence (str): Sentence to find the closest words.
            n_closest (int): Number of top closest words to find.

        Returns:
            List[str]: List of top closest word for given sentence.
        """
        neighbours = self.model.get_nearest_neighbors(sentence)
        words = [word for _, word in neighbours][:n_closest]
        return words

    def explain(self, word: str, n_words: int) -> List[str]:
        """Explain given word using specific number of words.

        Args:
            word (str): Word to explain.
            n_words (int): Number of words required in explanation.

        Returns:
            List[str]: Words to explain given word.
        """
        return self._find_words_for_sentence(word, n_words)

    def guess(self, words: List[str], n_words: int) -> List[str]:
        """Guess the word using provided list of explaining words.

        Args:
            words (List[str]): Explaining words.
            n_words (int): Required number of guesses.

        Returns:
            List[str]: Possible words for guessing.
        """
        return self._find_words_for_sentence(' '.join(words), n_words)


class LocalGensimPlayer(AbstractPlayer):
    """Local player based on gensim word2vec-based models."""

    def __init__(self, model: Any, hat_vocab: Set[str]) -> None:
        """Initialize player with fasttext model.

        Args:
            model (Any): Gensim model which will be used by player.
            hat_vocab (Set[str]): Possible words for the hat.
        """
        self.model = model
        self.hat_vocab = hat_vocab
        self.hat_vectors = self.prepare_hat_vectors()
        self.used_words: Set[str] = set()

    def prepare_hat_vectors(self) -> Word2VecKeyedVectors:
        """Prepare vectors from the hat vocabulary.

        Returns:
            Any: Model with prepared vectors.
        """
        hat_vocab_words = tuple(self.hat_vocab)
        hat_vectors = Word2VecKeyedVectors(self.model.vector_size)
        hat_vectors.add_vectors(hat_vocab_words, self.model.wv[hat_vocab_words])

        return hat_vectors

    @staticmethod
    def _filter_explanatory_words(secret_word: str, explanatory_words: List[str],
                                  min_word_length: int) -> List[str]:
        # Convert to lowercase
        words = [w.lower() for w in explanatory_words]

        # Remove all symbols except letters
        words = [re.sub(r'[\W\d]', '', w) for w in words]

        # Remove all words which have word being explained as a substring
        words = [w for w in words if secret_word not in w]

        # Remove all words with too small levenstein distance from the word being explained
        words = [w for w in words if distance(secret_word, w) > 2]

        # Remove all empty strings
        words = [w for w in words if w != '']

        # Remove all words which length is less than specific value
        words = [w for w in words if len(w) >= min_word_length]

        return words

    def _generate_explanatory_words(self, secret_word: str, n_closest: int) -> List[str]:
        """Find top closest explanatory words for given secret word.

        Args:
            secret_word (str): Sentence to find the closest words.
            n_closest (int): Number of top closest words to find.

        Returns:
            List[str]: List of top closest word for given sentence.
        """
        # Remove words which are not in model's vocabulary
        is_known_secret_word = (secret_word in self.model.wv)

        if is_known_secret_word:
            # Sort all filtered words by similarity in decreasing order
            vocab_size = n_closest * 10  # len(self.model.wv.index_to_key)
            neighbours = self.model.wv.most_similar([secret_word], topn=vocab_size)
            words = list(map(itemgetter(0), neighbours))

            # Get only similar words which are not equal to secret word
            words = [word for word in words if word != secret_word]

            # Filter words using hat game rules for the "soft" mode
            words = self._filter_explanatory_words(secret_word, words, min_word_length=2)

            # Get top closest remaining words as explanatory words
            words = words[:n_closest]
        else:
            words = []

        return words

    def _generate_guessing_words(self, explanatory_words: List[str], n_closest: int) -> List[str]:
        """Find top closest guessing words for given sentence.

        Args:
            explanatory_words (List[str]): Explanatory words which describe secret word.
            n_closest (int): Number of top closest guessing words to find.

        Returns:
            List[str]: List of top closest guessing words for given sentence.
        """
        # Reset set of already used words for new secret word
        if len(explanatory_words) == 1:
            self.used_words = set()

        # Preprocess explanatory words using the same rules as during the training
        # to match as many words as possible to the generated hat vocabulary
        # There is an assymetry in the game between preprocessing of explanatory words
        # and hat vocabulary generation
        processed_explanatory_words = convert_document_to_words(' '.join(explanatory_words),
                                                                min_word_length=2)

        # Remove words which are not in model's vocabulary
        filtered_words = [
            word for word in processed_explanatory_words if word in self.model.wv.key_to_index
        ]

        if filtered_words:
            # Sort all filtered words by similarity in decreasing order
            # Can be not optimized by speed without BLAS (e.g. on heroku)
            guessing_hat_vectors = deepcopy(self.hat_vectors)
            guessing_hat_vectors.add_vectors(filtered_words, self.model.wv[filtered_words])
            vocab_size = 1000  # len(guessing_hat_vectors.index_to_key)
            neighbours = guessing_hat_vectors.most_similar(filtered_words, topn=vocab_size)
            words = list(map(itemgetter(0), neighbours))

            # # If hat vocabulary is restricted
            # if self.hat_vocab:
            #     # Get only words which are in the hat vocabulary
            #     words = [word for word in words if word in self.hat_vocab]

            # Remove already used guessing words
            words = [word for word in words if word not in self.used_words]

            # Lemmatize and remove repeated words after lemmatization
            # words = remove_repeated_words([LEMMATIZER.lemmatize(word) for word in words[:top_n]])

            # Get only similar words which are not in explanatory words
            # words = [word for word in words if word not in set(processed_explanatory_words)]

            # Remove words which have edit distance > 2 to explanatory words
            # python-Levenshtein is used for distance calculation to speed up (~250x faster)
            # Only top_n words are used to speed up the response
            top_n = n_closest * 2
            words = [
                word for word in words[:top_n] if all(
                    distance(word, explanatory_word) > 2
                    for explanatory_word in set(filtered_words))
            ]

            # Get top closest remaining words as explanatory words
            words = words[:n_closest]
        else:
            words = []

        # Add current guessing words to used words set
        self.used_words.update(words)

        return words

    def explain(self, word: str, n_words: int) -> List[str]:
        """Explain given word using specific number of words.

        Args:
            word (str): Word to explain.
            n_words (int): Number of words required in explanation.

        Returns:
            List[str]: Words to explain given word.
        """
        return self._generate_explanatory_words(word, n_words)

    def guess(self, words: List[str], n_words: int) -> List[str]:
        """Guess the word using provided list of explaining words.

        Args:
            words (List[str]): Explaining words.
            n_words (int): Required number of guesses.

        Returns:
            List[str]: Possible words for guessing.
        """
        return self._generate_guessing_words(words, n_words)
