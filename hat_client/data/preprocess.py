"""Implements data preprocessing operations."""

import csv
from operator import attrgetter
from pathlib import Path
import re
import tarfile
from typing import List

from google.cloud import storage
import smart_open

from hat_client.text.utils import prepare_hat_vocab
from hat_client.utils.logger import get_logger


def preprocess_news_data(archive_path: Path,
                         output_file_path: Path,
                         input_encoding: str = 'utf-8',
                         output_encoding: str = 'utf-8') -> None:
    """Preprocess 20newsgroups dataset.

    Args:
        archive_path (Path): Archive with news dataset.
        output_file_path (Path): Preprocessed news file.
        input_encoding (str, optional): Encoding of news files. Defaults to 'utf-8'.
        output_encoding (str, optional): Encoding of preprocessed files. Defaults to 'utf-8'.
    """
    pattern = re.compile(r'\s+')

    with tarfile.open(archive_path, 'r') as tf:
        with open(output_file_path, 'w', encoding=output_encoding) as f_write:
            for entry in tf:
                if entry.isfile():
                    f = tf.extractfile(entry)
                    line = re.sub(pattern, ' ', f.read().decode(input_encoding))  # type: ignore
                    f_write.write(line + '\n')


def preprocess_gcs_data(gcs_bucket: str, output_file_path: Path, hat_vocab_path: Path) -> None:
    """Preprocess data from Google Cloud Storage.

    Args:
        gcs_bucket (str): GCS bucket name.
        output_file_path (Path): Preprocessed GCS data file.
        hat_vocab_path (Paht): Path to save hat vocabulary.
    """
    logger = get_logger('DATA_PREPROCESSING')
    storage_client = storage.Client.create_anonymous_client()
    blobs = sorted(storage_client.list_blobs(gcs_bucket), key=attrgetter('name'))
    # sample_size = 2**14
    last_blob_url = blobs[-1].public_url

    with open(output_file_path, 'w') as f_write:
        for blob in blobs:
            with smart_open.open(blob.public_url) as f:
                # Sniffer triggers false positive for has_header in single column file (text_7)
                # sniffer = csv.Sniffer()
                # has_header = sniffer.has_header(f.read(sample_size))
                first_row = f.readline().strip()
                has_header = (first_row == ',text')
                f.seek(0)

                if has_header:
                    reader = csv.DictReader(f)
                else:
                    reader = csv.DictReader(f, fieldnames=['text'])

                for row in reader:
                    f_write.write(row['text'] + '\n')

                blob_url = blob.public_url
                logger.info(f'File {blob_url} is preprocessed')

                if blob_url == last_blob_url:
                    hat_vocab_stats = prepare_hat_vocab(last_blob_url, hat_vocab_path)
                    logger.info(f'File {blob_url} was used to prepare hat vocabulary')
                    logger.info('Hat vocabulary statistics\n'
                                f'Vocabulary size: {hat_vocab_stats["vocab_size"]}\n'
                                f'Minimum word count: {hat_vocab_stats["min_count"]}\n'
                                f'Maximum word count: {hat_vocab_stats["max_count"]}')


def merge_data(file_paths: List[Path], output_file_path: Path) -> None:
    """Merge files.

    Args:
        file_paths (List[Path]): List of file paths to merge.
        output_file_path (Path): File path to save merged file.
    """
    with open(output_file_path, 'w') as f:
        for file_path in file_paths:
            for line in open(file_path):
                f.write(line)
