"""Implements functions related to model training."""

from pathlib import Path
from typing import Any

from box import ConfigBox
import fasttext
from gensim.models.fasttext import FastText, Word2Vec

from hat_client.utils.logger import EpochLogger, get_logger


def train_fasttext_model(data_path: Path, config: ConfigBox) -> Any:
    """Train model using fasttext package.

    Args:
        data_path (Path): Path to dataset for training.
        config (ConfigBox): Configuration settings.

    Returns:
        Any: [description]
    """
    params = {
        'model': config.model_training.model_algorithm,
        'dim': config.model_training.dim,
        'minn': config.model_training.min_n,
        'maxn': config.model_training.max_n,
        'lr': config.model_training.lr,
        'epoch': config.model_training.epoch,
        'minCount': config.model_training.min_count,
        'ws': config.model_training.window_size
    }
    model = fasttext.train_unsupervised(str(data_path), **params)

    return model


def train_gensim_model(model_type: str, data_path: Path, config: ConfigBox) -> Any:
    """Train model using gensim package.

    Args:
        model_type (str): Type of the model to train (fasttext or word2vec)
        data_path (Path): Path to dataset for training.
        config (ConfigBox): Configuration settings.

    Returns:
        Any: Trained model.
    """
    logger = get_logger('GENSIM_MODEL')

    if config.model_training.model_algorithm not in {'cbow', 'skipgram'}:
        raise ValueError('Model algorithm can be cbow or skipgram')

    model_params = {
        'vector_size': config.model_training.dim,
        'epochs': config.model_training.epoch,
        'alpha': config.model_training.lr,
        'sg': int(config.model_training.model_algorithm == 'skipgram'),
        'hs': config.model_training.hs,
        'negative': config.model_training.negative,
        'cbow_mean': config.model_training.cbow_mean,
        'min_count': config.model_training.min_count,
        'window': config.model_training.window_size,
        'workers': config.model_training.workers
    }

    if model_type == 'word2vec':
        model = Word2Vec(**model_params)
    elif model_type == 'fasttext':
        model = FastText(**model_params)
    else:
        raise ValueError('Model type can be word2vec or fasttext')

    model.build_vocab(corpus_file=str(data_path))
    vocab_size = len(model.wv.index_to_key)
    logger.info(f'Vocabulary size of the model: {vocab_size}')

    epoch_logger = EpochLogger()
    train_params = {
        'epochs': model.epochs,
        'total_examples': model.corpus_count,
        'total_words': model.corpus_total_words,
        'compute_loss': True,
        'callbacks': [epoch_logger],
    }
    model.train(corpus_file=str(data_path), **train_params)

    return model


def train_model(package_name: str, model_type: str, data_path: Path, config: ConfigBox) -> Any:
    """Train model.

    Args:
        package_name (str): Package name to use for model saving (fasttext or gensim).
        model_type (str): Type of the model to train (word2vec or fasttext)
        data_path (Path): Path to dataset for training.
        config (ConfigBox): Configuration settings.

    Returns:
        Any: Trained model.
    """
    if package_name == 'fasttext':
        model = train_fasttext_model(data_path, config)
    elif package_name == 'gensim':
        model = train_gensim_model(model_type, data_path, config)
    else:
        raise ValueError('Package name can be fasttext or gensim')

    return model
