hat\_client package
===================

.. automodule:: hat_client
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   hat_client.app
   hat_client.data
   hat_client.game
   hat_client.models
   hat_client.pipelines
   hat_client.text
   hat_client.utils
