hat\_client.app package
=======================

.. automodule:: hat_client.app
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.app.api module
--------------------------

.. automodule:: hat_client.app.api
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.app.settings module
-------------------------------

.. automodule:: hat_client.app.settings
   :members:
   :undoc-members:
   :show-inheritance:
