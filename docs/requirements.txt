m2r2==0.2.7
sphinx-rtd-theme==0.5.2
sphinx==4.0.2; python_version >= "3.6"
sphinxcontrib-applehelp==1.0.2; python_version >= "3.6"
sphinxcontrib-devhelp==1.0.2; python_version >= "3.6"
sphinxcontrib-htmlhelp==2.0.0; python_version >= "3.6"
sphinxcontrib-jsmath==1.0.1; python_version >= "3.6"
sphinxcontrib-napoleon==0.7
sphinxcontrib-qthelp==1.0.3; python_version >= "3.6"
sphinxcontrib-serializinghtml==1.1.5; python_version >= "3.6"

gensim==4.0.1; python_version >= "3.6"
google-cloud-storage==1.36.2; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.6.0")
fasttext==0.9.2
flask==2.0.1; python_version >= "3.6"
nltk==3.6.2; python_version >= "3.8" and python_version < "4.0"
python-box==5.3.0; python_version >= "3.6"
spacy==3.0.6; python_version >= "3.6"
