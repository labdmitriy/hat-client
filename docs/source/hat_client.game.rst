hat\_client.game package
========================

.. automodule:: hat_client.game
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.game.players module
-------------------------------

.. automodule:: hat_client.game.players
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.game.simulator module
---------------------------------

.. automodule:: hat_client.game.simulator
   :members:
   :undoc-members:
   :show-inheritance:
