.. The Hat Game Client documentation master file, created by
   sphinx-quickstart on Sun May 30 21:12:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The Hat Game Client's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
