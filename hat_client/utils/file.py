"""File operations."""
from pathlib import Path


def get_project_root() -> Path:
    """Return path of the project root folder.

    Returns:
        Path: Absolute path of the project root folder.
    """
    return Path(__file__).parents[2]
