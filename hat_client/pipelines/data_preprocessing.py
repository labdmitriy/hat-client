"""Implements data loading and extraction pipeline."""

import argparse
from pathlib import Path

from box import ConfigBox

from hat_client.data.preprocess import merge_data, preprocess_gcs_data
from hat_client.utils.config import load_config
from hat_client.utils.logger import get_logger


def data_preprocessing(config: ConfigBox) -> None:
    """Data preprocessing pipeline.

    Args:
        config (ConfigBox): Configuration settings.
    """
    logger = get_logger('DATA_PREPROCESSING', config.base.log_level)

    file_paths = []

    # logger.info('Download and preprocess news data')
    # news_url = config.data_preprocessing.news_url
    # download_dir = Path(config.data_preprocessing.download_dir)
    # news_archive_path = download_data(news_url, download_dir)
    # news_file_path = Path(config.data_preprocessing.output_news_file)
    # preprocess_news_data(news_archive_path, news_file_path, input_encoding='latin-1')
    # logger.info(f'Preprocessed news file is saved to {news_file_path}')
    # file_paths.append(news_file_path)

    logger.info('Download and preprocess GCS data')
    gcs_bucket = config.data_preprocessing.gcs_bucket
    gcs_file_path = Path(config.data_preprocessing.output_gcs_file)
    hat_vocab_path = Path(config.data_preprocessing.hat_vocab_file)
    preprocess_gcs_data(gcs_bucket, gcs_file_path, hat_vocab_path)
    logger.info(f'Preprocessed GCS file is saved to {gcs_file_path}')
    logger.info(f'Created vocabulary for the hat is saved to {hat_vocab_path}')
    file_paths.append(gcs_file_path)

    logger.info('Merge preprocessed datasets')
    merged_file_path = Path(config.data_preprocessing.output_file)
    merge_data(file_paths, merged_file_path)
    logger.info(f'Merged file is saved to {merged_file_path}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    config_path = Path(args.config)
    config = load_config(config_path)

    data_preprocessing(config)
