"""Implements data input/output operations."""

from pathlib import Path

import requests


def download_data(url: str, download_dir: Path, chunk_size: int = 8192) -> Path:
    """Stream downloading file by URL to directory.

    Args:
        url (str): URL of the downloaded file.
        download_dir (Path): Directory to save the file.
        chunk_size (int): Chunk size for the stream. Defaults to 8192.

    Returns:
        Path: Downloaded file path.
    """
    file_name = url.split('/')[-1]
    download_dir.mkdir(parents=True, exist_ok=True)
    download_file_path = download_dir / file_name

    if not download_file_path.exists():
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(download_file_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size):
                    f.write(chunk)

    return download_file_path
