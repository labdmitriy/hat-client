"""Implements model's training pipeline."""

import argparse
from pathlib import Path

from box import ConfigBox

from hat_client.models.io import save_model
from hat_client.models.train import train_model
from hat_client.utils.config import load_config
from hat_client.utils.logger import get_logger


def model_training(config: ConfigBox) -> None:
    """Model training pipeline.

    Args:
        config (ConfigBox): Configuration settings.
    """
    logger = get_logger('MODEL_TRAINING', config.base.log_level)

    logger.info('Train model')
    package_name = config.model_training.package_name
    model_type = config.model_training.model_type
    data_path = Path(config.text_processing.output_file)
    model = train_model(package_name, model_type, data_path, config)

    logger.info('Save model')
    output_file_path = Path(config.model_training.output_file)
    save_model(package_name, model, output_file_path)
    logger.info(f'File is saved to {output_file_path}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    config_path = Path(args.config)
    config = load_config(config_path)

    model_training(config)
