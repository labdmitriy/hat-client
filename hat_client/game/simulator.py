"""Implements gameplay process."""

import argparse
from pathlib import Path

from box import ConfigBox
from the_hat_game.game import Game

from hat_client.app.settings import setup_app
from hat_client.game.players import (LocalFasttextPlayer, LocalGensimPlayer, PlayerDefinition,
                                     RemotePlayer)
from hat_client.utils.config import load_config


def play_game(config: ConfigBox) -> None:
    """Simulate game using multiple players.

    Args:
        config (ConfigBox): Configuration settings.
    """
    criteria = config.game.criteria
    n_rounds = config.game.n_rounds
    n_explain_words = config.game.n_explain_words
    n_guessing_words = config.game.n_guessing_words
    complete = config.game.complete
    each_game = config.game.report_each_game
    random_state = config.base.random_state

    # fasttext_model = load_model('fasttext', Path('models/model.bin'))

    settings = setup_app()
    model = settings['model']
    hat_vocab = settings['hat_vocab']

    all_words = []
    for vocab_path in config.game.word_files:
        with open(vocab_path) as f:
            words = f.readlines()
            words = [word.strip() for word in words]
            all_words.extend(words)
    print(f'Words we will use for the game: {sorted(all_words)}')

    players = [
        # PlayerDefinition('Team 1', LocalFasttextPlayer(fasttext_model)),
        # PlayerDefinition('Team 2', LocalFasttextPlayer(fasttext_model)),
        PlayerDefinition('Team 3', LocalGensimPlayer(model, hat_vocab)),
        PlayerDefinition('Team 4', LocalGensimPlayer(model, hat_vocab))
        # PlayerDefinition('Remote Team 1', RemotePlayer('http://10.10.10.10:5000/'))
        # PlayerDefinition('Heroku Team 1', RemotePlayer('https://hat-client.herokuapp.com/'))
    ]

    game = Game(players, all_words, criteria, n_rounds, n_explain_words, n_guessing_words,
                random_state)
    game.run(verbose='print_logs', complete=complete)
    game.report_results(each_game=each_game)


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    config_path = Path(args.config)
    config = load_config(config_path)

    play_game(config)
