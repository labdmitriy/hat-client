"""Implements model input/output operations."""

from pathlib import Path
import pickle
from typing import Any, Set

import fasttext
from gensim.models.fasttext import FastText


def save_model(package_name: str, model: Any, file_path: Path) -> None:
    """Save model to the file.

    Args:
        package_name (str): Package name to use for model saving.
        model (Any): Model to save.
        file_path (Path): File path where to save the model.
    """
    if package_name == 'fasttext':
        model.save_model(str(file_path))
    elif package_name == 'gensim':
        model.save(str(file_path))
    else:
        raise ValueError('Package name can be fasttext or gensim')


def load_model(package_name: str, file_path: Path) -> Any:
    """Load fasttext model from the file.

    Args:
        package_name (str): Package name to use for model saving.
        file (Path): File with model to load.

    Returns:
        Any: Loaded model.
    """
    if package_name == 'fasttext':
        model = fasttext.load_model(str(file_path))
    elif package_name == 'gensim':
        model = FastText.load(str(file_path))
    else:
        raise ValueError('Package name can be fasttext or gensim')

    return model


def load_vocab(vocab_path: Path) -> Set:
    """Load vocabulary for the model.

    Args:
        vocab_path (Path): Path to the file with vocabulary.

    Returns:
        Set: Vocabulary words.
    """
    with open(vocab_path, 'rb') as f:
        vocab = pickle.load(f)

    return vocab
