"""Implements Flask application for client API."""

import json

from flask import Flask, request

from hat_client.app.settings import app_config
from hat_client.game.players import LocalGensimPlayer

app = Flask(__name__)

model = app_config['model']
hat_vocab = app_config['hat_vocab']

player = LocalGensimPlayer(model, hat_vocab)


@app.route('/explain')
def explain() -> str:
    """Explain given word using specific number of words.

    Returns:
        str: List of words explaining given word.
    """
    word = request.args.get('word', '')
    n_words = int(request.args.get('n_words', 0))

    if word and n_words > 0:
        explanatory_words = json.dumps(player.explain(word, n_words))
    else:
        explanatory_words = json.dumps([])

    return explanatory_words


@app.route('/guess')
def guess() -> str:
    """Guess the word using provided list of explaining words.

    Player provides specific number of guesses.

    Returns:
        str: List of guessing words.
    """
    words = request.args.getlist('words')
    n_words = int(request.args.get('n_words', 0))

    if words and n_words > 0:
        guessing_words = json.dumps(player.guess(words, n_words))
    else:
        guessing_words = json.dumps([])

    return guessing_words
