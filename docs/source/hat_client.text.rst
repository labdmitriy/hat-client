hat\_client.text package
========================

.. automodule:: hat_client.text
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.text.process module
-------------------------------

.. automodule:: hat_client.text.process
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.text.utils module
-----------------------------

.. automodule:: hat_client.text.utils
   :members:
   :undoc-members:
   :show-inheritance:
