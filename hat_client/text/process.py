"""Implements text processing operations."""

from pathlib import Path

import spacy

from hat_client.text.utils import convert_document_to_words


def process_spacy_text(file_path: Path, output_file_path: Path) -> None:
    """Process text with spacy and save to file.

    Args:
        file_path (Path): Path of the file to process.
        output_file_path (Path): Path of the file to save after processing.
    """
    all_components = set(
        ['tok2vec', 'senter', 'ner', 'tagger', 'attribute_ruler', 'lemmatizer', 'parser'])
    selected_components = set(['senter', 'lemmatizer', 'tagger', 'attribute_ruler'])
    disabled_components = all_components - selected_components
    nlp = spacy.load('en_core_web_sm', disable=disabled_components)

    with open(file_path) as f, open(output_file_path, 'w') as f_write:
        for line in f:
            doc = nlp(line)
            processed_doc = [token.lemma_ for token in doc if token.is_alpha and not token.is_stop]
            f_write.write(' '.join(processed_doc) + '\n')


def process_nltk_text(file_path: Path, output_file_path: Path) -> None:
    """Process text with nltk and save to file.

    Args:
        file_path (Path): Path of the file to process.
        output_file_path (Path): Path of the file to save after processing.
    """
    with open(output_file_path, 'w') as f_write:
        for doc in open(file_path, 'r'):
            words = convert_document_to_words(doc, min_word_length=2)
            f_write.write(' '.join(words) + '\n')
