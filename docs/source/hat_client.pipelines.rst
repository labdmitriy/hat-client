hat\_client.pipelines package
=============================

.. automodule:: hat_client.pipelines
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.pipelines.data\_preprocessing module
------------------------------------------------

.. automodule:: hat_client.pipelines.data_preprocessing
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.pipelines.model\_training module
--------------------------------------------

.. automodule:: hat_client.pipelines.model_training
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.pipelines.text\_processing module
---------------------------------------------

.. automodule:: hat_client.pipelines.text_processing
   :members:
   :undoc-members:
   :show-inheritance:
