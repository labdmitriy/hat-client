"""Provides functions to create loggers."""

import logging
from pathlib import Path
import sys
import time
from typing import Any, List, Text, Union

from gensim.models.callbacks import CallbackAny2Vec

from hat_client.models.io import save_model


def get_console_handler() -> logging.StreamHandler:
    """Get console handler.

    Returns:
        logging.StreamHandler: Handler which logs into stdout.
    """
    console_handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s — %(name)s — %(levelname)s — %(message)s')
    console_handler.setFormatter(formatter)

    return console_handler


def get_logger(name: Text = __name__,
               log_level: Union[Text, int] = logging.DEBUG) -> logging.Logger:
    """Get logger.

    Args:
        name (Text, optional): Logger name. Defaults to __name__.
        log_level (Union[Text, int], optional): Logging level. Defaults to logging.DEBUG.

    Returns:
        logging.Logger: Logger instance
    """
    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    # Prevent duplicate outputs in Jypyter Notebook
    if logger.hasHandlers():
        logger.handlers.clear()

    logger.addHandler(get_console_handler())
    logger.propagate = False

    return logger


class EpochLogger(CallbackAny2Vec):
    """Implements logging of progress and model during training."""

    def __init__(self,
                 package_name: str = 'gensim',
                 model_dir: Path = None,
                 epochs_to_save: int = 0) -> None:
        """Initializes epoch information.

        Args:
            package_name (str, optional): Package used for the model. Defaults to 'gensim'.
            model_dir (Path, optional): Directory for model saving. Defaults to None.
            epochs_to_save (int, optional): Number of epochs before saving. Defaults to 0.
        """
        self.package_name = package_name
        self.model_dir = model_dir
        self.epochs_to_save = epochs_to_save
        self.epoch = 1
        self.cumulative_loss = 0
        self.losses: List[float] = []
        self.previous_epoch_time = time.time()
        self.logger = get_logger('EPOCH_LOGGER')

    def on_train_begin(self, model: Any) -> None:
        """Actions before training."""
        self.logger.info('Training is started.')

    def on_train_end(self, model: Any) -> None:
        """Actions after training."""
        self.logger.info('Training is ended.')

    def on_epoch_end(self, model: Any) -> None:
        """Actions after each epoch."""
        current_loss = model.get_latest_training_loss()

        now = time.time()
        epoch_seconds = now - self.previous_epoch_time
        self.previous_epoch_time = now
        self.cumulative_loss += current_loss

        self.logger.info(f'Loss after epoch {self.epoch}: {current_loss} '
                         f'(cumulative loss: {self.cumulative_loss}) '
                         f'-> epoch took {round(epoch_seconds, 2)} s')
        self.epoch += 1
        self.losses.append(current_loss)
        model.running_training_loss = 0.0

        if self.model_dir and self.epochs_to_save > 0 and self.epoch % self.epochs_to_save == 0:
            save_model(self.package_name, model, self.model_dir)
