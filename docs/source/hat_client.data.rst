hat\_client.data package
========================

.. automodule:: hat_client.data
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

hat\_client.data.io module
--------------------------

.. automodule:: hat_client.data.io
   :members:
   :undoc-members:
   :show-inheritance:

hat\_client.data.preprocess module
----------------------------------

.. automodule:: hat_client.data.preprocess
   :members:
   :undoc-members:
   :show-inheritance:
