from setuptools import find_packages, setup

setup(
    name='hat_client',
    packages=find_packages(),
    version='0.1.0',
    description='Client for the Hat game',
    author='Dmitry Labazkin',
)
